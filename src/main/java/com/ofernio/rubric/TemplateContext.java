package com.ofernio.rubric;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

import java.io.*;

/**
 * Created by mnasyrov on 4/6/2015.
 */
public class TemplateContext extends VelocityContext {

    private static Logger logger = LogManager.getLogger(TemplateContext.class);
    private Template template;
    private File outputPath;
    private StringWriter writer = new StringWriter();
    private BufferedWriter fileWriter;
    private DBProvider dbProvider;

    public void init(Template template, File outputPath, DBProvider dbProvider) {
        this.template = template;
        this.outputPath = outputPath;
        this.dbProvider = dbProvider;
    }

    public DBProvider db() {
        return dbProvider;
    }

    public void process() throws IOException {
        logger.info(String.format("Processing template '%s' ...", template.getName()));
        if(!getGenerationPath().exists())
            getGenerationPath().mkdirs();
        FileUtils.cleanDirectory(getGenerationPath());

        put("context", this);
        template.merge(this, writer);
    }

    private File getGenerationPath() {
        return outputPath;
    }

    public void startFile(String fileName) throws IOException {
        this.writer.getBuffer().setLength(0);
        File generationPath = this.getGenerationPath();
        generationPath.mkdirs();
        this.fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(generationPath, fileName)), "UTF-8"));
    }

    public void closeFile() throws IOException {
        this.fileWriter.write(this.writer.toString());
        this.fileWriter.close();
        this.fileWriter = null;
    }
}
