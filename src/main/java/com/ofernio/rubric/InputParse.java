package com.ofernio.rubric;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.util.NumberToTextConverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mnasyrov on 2/12/2016.
 */
public class InputParse {
    public static List<Map<String, String>> parse(File file, Map<Integer, String> fields) throws IOException {
        POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(file));
        HSSFWorkbook wb = new HSSFWorkbook(fs);
        HSSFSheet sheet = wb.getSheetAt(0);
        int rows = sheet.getPhysicalNumberOfRows();
        List<Map<String, String>> result = new ArrayList<>();
        for(int i = 1; i < rows; i++) {
            HSSFRow row = sheet.getRow(i);
            Map<String, String> rowResult = new HashMap<>();
            for (Map.Entry<Integer, String> field : fields.entrySet()) {
                HSSFCell cell = row.getCell(Integer.parseInt(String.valueOf(field.getKey())));
                String cellVal = cell == null ? "" : (cell.getCellType() == 0 ? NumberToTextConverter.toText(cell.getNumericCellValue()) : cell.toString());
                rowResult.put(field.getValue(), cellVal);
            }
            result.add(rowResult);
        }
        return result;
    }
}
