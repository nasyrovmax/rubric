package com.ofernio.rubric;

import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import java.io.*;
import java.util.*;

/**
 * Created by mnasyrov on 3/11/2016.
 */
public class DBProvider {
    private final SQLContext sqlContext;
    private final File basePath;
    public DBProvider(SQLContext sqlContext, File basePath) {
        this.sqlContext = sqlContext;
        this.basePath = basePath;
    }

    public void createTempViewFromJSON(String name, String json) throws IOException, AnalysisException {
        File tempFile = new File("temp", UUID.randomUUID().toString());
        try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempFile), "UTF-8"))) {
            writer.write(json);
        }

        createTempView(name, tempFile);
    }

    public void createTempViewFromQuery(String name, String query) throws AnalysisException {
        sqlContext.sql(query).createTempView(name);
    }

    public void createTempView(String name, String fileName) throws IOException, AnalysisException {
        File tempFile = new File(basePath, fileName);
        createTempView(name, tempFile);
    }

    public void createTempView(String name, File file) throws IOException, AnalysisException {
        Dataset<Row> dataFrame = sqlContext.read().json(file.getPath());
        dataFrame.createTempView(name);
        dataFrame.cache();
    }

    public List<Map<String, String>> sql(String queryText) {
        Dataset<Row> frame = sqlContext.sql(queryText);
        String[] fields = frame.schema().fieldNames();

        List<Map<String, String>> result = new ArrayList<>();
        for (Row row : frame.collectAsList()) {
            Map<String, String> r = new HashMap<>();
            for (String field : fields) {
                r.put(field, row.getAs(field));
            }

            result.add(r);
        }
        return result;
    }
}
