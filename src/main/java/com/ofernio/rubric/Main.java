package com.ofernio.rubric;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.SQLContext;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.FileResourceLoader;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws IOException, SQLException, ParserConfigurationException, SAXException, AnalysisException {

        cleanFolder(new File("temp"));

        logger.info("Loading configs ...");
        ObjectMapper mapper = new ObjectMapper();
        Map<Integer, String> fields = mapper.readValue(getConfig("fields.json"), Map.class);

        logger.info("Loading input file ...");
        File inputfile = getInputFile("input");
        logger.info("Parsing input file ...");
        List<Map<String, String>> parsedData = InputParse.parse(inputfile, fields);

        SparkConf conf = new SparkConf().setAppName("com.ofernio.rubric").setMaster("local");
        JavaSparkContext context = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(context);
        sqlContext.sql("SET spark.sql.shuffle.partitions=4").show();
        sqlContext.sql("SET spark.executor.memory=1g").show();

        //processing templates
        File templatesRootPath = new File("templates");
        File outputPath = new File("results");
        cleanFolder(outputPath);

        DBProvider dbProvider = new DBProvider(sqlContext, templatesRootPath);
        dbProvider.createTempViewFromJSON("INPUT_RAW", mapper.writeValueAsString(parsedData));

        VelocityEngine velocityEngine = initVelocityEngine(templatesRootPath);
        for (File templateFile : getTemplates(templatesRootPath)) {
            String templatePath = templatesRootPath.toPath().relativize(templateFile.toPath()).toString();
            logger.info(String.format("Processing template '%s' ...", templatePath));
            Template template = velocityEngine.getTemplate(templatePath, StandardCharsets.UTF_8.name());

            TemplateContext templateContext = new TemplateContext();
            templateContext.init(template, outputPath, dbProvider);
            templateContext.process();
        }

        logger.info("Templates processing success!");
        Desktop.getDesktop().browse(new File(outputPath, "result.html").toURI());
        Desktop.getDesktop().browse(new File(outputPath, "error.html").toURI());
        System.out.println("Press any key to exit");
        System.in.read();
    }

    private static List<File> getTemplates(File rootPath) {
        List<File> result = new ArrayList<>();

        for (File file : rootPath.listFiles()) {
            if(file.getName().endsWith(".vm")) {
                result.add(file);
            }
        }
        return result;
    }

    private static File getInputFile(String path) {
        ArrayList<String> files = new ArrayList<>(Arrays.asList((new File(path).list())));
        String filePath = files.stream().filter(c -> c.endsWith(".xls") || c.endsWith(".xlsx")).findFirst().orElse(null);
        if(filePath == null)
            throw new RuntimeException("Input file not found");
        return new File(path, filePath);
    }

    private static String getConfig(String name) throws IOException {
        File file = new File("config", name);
        if(!file.exists())
            throw new RuntimeException(String.format("Config '%s' not found", name));
        List<String> strings = Files.readAllLines(file.toPath(), Charset.forName("UTF-8"));
        String data = "";
        for (String string : strings) {
            data += string;
        }
        return data.replaceAll("\\t", "").replaceAll("\\n", "");
    }

    private static VelocityEngine initVelocityEngine(File sourcePath) {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.addProperty(RuntimeConstants.RESOURCE_LOADER, "file");
        velocityEngine.addProperty("file.resource.loader.class", FileResourceLoader.class.getName());
        String rootPath = sourcePath.getPath();
        velocityEngine.addProperty("file.resource.loader.path", rootPath);
        logger.info(String.format("Templates root path '%s'", rootPath));

        velocityEngine.addProperty("runtime.log.logsystem.class", org.apache.velocity.runtime.log.NullLogChute.class.getName());
        velocityEngine.init();
        return velocityEngine;
    }

    private static void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if(files!=null) { //some JVMs return null for empty dirs
            for(File f: files) {
                if(f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }

    private static void cleanFolder(File folder) {
        deleteFolder(folder);
        folder.mkdir();
    }
}
